package me.saiintbrisson.playground.utils;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.concurrent.Callable;

public class TaskHelper {

	private static Plugin instance;
	public TaskHelper(Plugin plugin) {
		instance = plugin;
	}

	public static void callAsync(Runnable runnable) {
		Bukkit.getScheduler().runTaskAsynchronously(instance, runnable);
	}

	public static <T> void callSync(Callable<T> callable) {
		Bukkit.getScheduler().callSyncMethod(instance, callable);
	}

	public static BukkitTask scheduleRepeatingTask(Runnable runnable, long period,
												   long delay, boolean async) {
		if(!async) {
			return Bukkit.getScheduler().runTaskTimer(instance, runnable, delay, period);
		} else {
			return Bukkit.getScheduler().runTaskTimerAsynchronously(instance, runnable, delay, period);
		}
	}

	public static BukkitTask scheduleDelayingTask(Runnable runnable, long delay, boolean async) {
		if(!async) {
			return Bukkit.getScheduler().runTaskLater(instance, runnable, delay);
		} else {
			return Bukkit.getScheduler().runTaskLaterAsynchronously(instance, runnable, delay);
		}
	}

}
