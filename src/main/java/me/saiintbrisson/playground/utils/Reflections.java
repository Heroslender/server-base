package me.saiintbrisson.playground.utils;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;

import java.lang.reflect.Field;

public class Reflections {

	public static boolean setField(Object instance, String fieldName, Object value) {
		try {
			Field field = instance.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			field.set(instance, value);

			return true;
		} catch (Exception e) {
			return false;
		}
	}


	public static <T> T getField(Object instance, String fieldName) {
		try {
			Field field = instance.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);

			return (T) field.get(instance);
		} catch (Exception e) {
			return null;
		}
	}

	public static CommandMap getCommandMap() {
		return getField(Bukkit.getServer(), "commandMap");
	}

}
