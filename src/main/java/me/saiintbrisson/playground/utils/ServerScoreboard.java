package me.saiintbrisson.playground.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import me.saiintbrisson.playground.cache.Cache;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.security.SecureRandom;
import java.util.function.Function;

@RequiredArgsConstructor
public class ServerScoreboard {

	private final static String COLOR_CHARS = "1234567890fbcead";

	private final static RandomString RANDOM_STRING = new RandomString(16);
	private final static RandomString RANDOM_COLOR = new RandomString(
			1,
			new SecureRandom(),
			COLOR_CHARS
	);

	private static String generateRandomColors() {
		return "§" + RANDOM_COLOR.nextString() + "§" + RANDOM_COLOR.nextString();
	}

	@NonNull
	private String name, title;
	@NonNull
	private DisplaySlot displaySlot;

	@Getter
	private final Cache<ScoreboardLine> lineCache = new Cache<ScoreboardLine>() {};

	public ScoreboardLine addLine() {
		return addLine(null, RANDOM_STRING.nextString(), generateRandomColors(), null);
	}

	public ScoreboardLine addLine(String line) {
		return addLine(null, RANDOM_STRING.nextString(), line, null);
	}

	public ScoreboardLine addLine(Integer slot) {
		return addLine(slot, RANDOM_STRING.nextString(), generateRandomColors(), null);
	}

	public ScoreboardLine addLine(String name, String line) {
		return addLine(null, name, line, null);
	}

	public ScoreboardLine addLine(Integer slot, String line) {
		return addLine(null, RANDOM_STRING.nextString(), line, null);
	}

	public ScoreboardLine addLine(Integer slot, String name, String line) {
		return addLine(slot, name, line, null);
	}

	public ScoreboardLine addLine(String name, String line, String suffix) {
		return addLine(null, name, line, suffix);
	}

	public ScoreboardLine addLine(Integer slot, String name, String line, String suffix) {
		ScoreboardLine scoreboardLine = new ScoreboardLine(name, line);
		scoreboardLine.setSlot(slot);
		scoreboardLine.setSuffix(suffix);
		lineCache.addElement(scoreboardLine);
		return scoreboardLine;
	}

	public void show(Player player) {
		Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective objective = scoreboard.registerNewObjective(name, "dummy");
		objective.setDisplayName(title);
		objective.setDisplaySlot(displaySlot);

		int currentSlot = 0;
		for (int i = lineCache.getElements().size(); i > 0; i--) {
			ScoreboardLine line = lineCache.getByIndex(i - 1);
			Team team = scoreboard.registerNewTeam(line.name);
			team.addEntry(line.line);

			if(line.function != null) {
				String rawSuffix = line.function.apply(player);
				if(rawSuffix.length() > 16) rawSuffix = rawSuffix.substring(0, 16);
				String suffix = rawSuffix;
				team.setSuffix(suffix);
			} else if(line.suffix != null) {
				String rawSuffix = line.suffix;
				if(rawSuffix.length() > 16) rawSuffix = rawSuffix.substring(0, 16);
				String suffix = rawSuffix;
				team.setSuffix(suffix);
			}

			if(line.slot != null) {
				objective.getScore(line.line).setScore(line.slot + currentSlot);
			} else {
				objective.getScore(line.line).setScore(i);
				currentSlot++;
			}
		}

		player.setScoreboard(scoreboard);
	}

	public void update(Player player) {
		for (ScoreboardLine element : lineCache.getElements()) {
			update(player, element.name);
		}
	}

	public void update(Player player, String name) {
		Scoreboard scoreboard = player.getScoreboard();
		if(scoreboard == null) return;

		ScoreboardLine line = lineCache.get(it -> it.name.equals(name));
		if(line == null) return;

		Team team = scoreboard.getTeam(line.name);
		if(team == null) return;

		if(line.function == null) return;
		String rawSuffix = line.function.apply(player);
		if(rawSuffix.length() > 16) rawSuffix = rawSuffix.substring(0, 16);
		String suffix = rawSuffix;

		team.setSuffix(suffix);
	}

	@Getter
	@RequiredArgsConstructor
	public class ScoreboardLine {

		@Setter
		private Integer slot;

		@NonNull
		private String name, line;

		@Setter
		private String suffix;

		@Setter
		private Function<Player, String> function;

	}

}
