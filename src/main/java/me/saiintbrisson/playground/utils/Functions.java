package me.saiintbrisson.playground.utils;

import java.util.function.Consumer;

public class Functions {


   public static <T> T apply(T object, Consumer<T> consumer) {
	  ifNotNull(object, consumer);
	  return object;
   }

   /**
	* Executes a action if object is not null.
	*
	* @param obj      object
	* @param consumer action
	*/
   public static <T> void ifNotNull(T obj, Consumer<T> consumer) {
	  if (obj == null) {
		 return;
	  }

	  consumer.accept(obj);
   }

}
