package me.saiintbrisson.playground.utils;

import lombok.Getter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class DateUtils {

   private static final int SECOND = 1000;
   private static final int MINUTE = SECOND * 60;
   private static final int HOUR = MINUTE * 60;
   private static final int DAY = HOUR * 24;
   private static final long WEEK = DAY * 7;
   private static final long MONTH = WEEK * 30;
   private static final long YEAR = MONTH * 365;
   
   @Getter
   private Calendar calendar;
   @Getter
   private long ms;

   public DateUtils(TimeZones timeZone) {
	  calendar = new GregorianCalendar();
	  calendar.setTime(new Date());
	  calendar.setTimeZone(TimeZone.getTimeZone(timeZone.timeZone()));
   }

   public DateUtils(long milli) {
	  calendar = new GregorianCalendar();
	  calendar.setTimeInMillis(milli);
	  ms = milli;
   }

   public DateUtils(Calendar calendar) {
	  this.calendar = calendar;
   }

   public DateUtils(Date date) {
	  calendar = new GregorianCalendar();
	  calendar.setTime(date);
   }

   public DateUtils() {
	  this.calendar = Calendar.getInstance();
   }

   public static DateUtils parse(String pattern, String date) throws ParseException {
	  return new DateUtils(new SimpleDateFormat(pattern).parse(date));
   }

   public void updateTo(long milli) {
	  calendar = new GregorianCalendar();
	  calendar.setTimeInMillis(milli);
	  ms = milli;
   }

   public int getHour() {
	  return calendar.get(Calendar.HOUR_OF_DAY);
   }

   public int getMinute() {
	  return calendar.get(Calendar.MINUTE);
   }

   public int getSecond() {
	  return calendar.get(Calendar.SECOND);
   }

   public int getDay() {
	  return calendar.get(Calendar.DAY_OF_MONTH);
   }

   public int getMonth() {
	  return calendar.get(Calendar.MONTH);
   }

   public int getYear() {
	  return calendar.get(Calendar.YEAR);
   }

   public String format(String pattern) {
	  return new SimpleDateFormat(pattern).format(calendar.getTime());
   }

   public String format() {
	  return format("MM/dd/yyyy - HH:mm:ss");
   }

   public long getRemainingYears() {
	  return ms / YEAR;
   }

   public long getRemainingMonths() {
	  return ms / MONTH;
   }

   public long getRemainingWeeks() {
	  return ms / WEEK;
   }

   public int getRemainingDays() {
	  return (int) ((ms % WEEK) / DAY);
   }

   public int getRemainingHours() {
	  return (int) ((ms % DAY) / HOUR);
   }

   public int getRemainingMinutes() {
	  return (int) ((ms % HOUR) / MINUTE);
   }

   public int getRemainingSeconds() {
	  return (int) ((ms % MINUTE) / SECOND);
   }

   public enum TimeZones {
	  AUSTRALIA("Australia/Canberra"),
	  AUSTRALIA_MELBOURNE("Australia/Melbourne"),
	  AUSTRALIA_SYDNEY("Australia/Sydney"),
	  US("America/New_York"),
	  US_LOSANGELES("America/Los_Angeles"),
	  ENGLAND("Europe/London"),
	  PORTUGAL("Europe/Lisbon"),
	  BRAZIL("America/Sao_Paulo"),
	  BRAZIL_ACRE("Brazil/Acre"),
	  BRAZIL_BAHIA("America/Bahia"),
	  BRAZIL_BELEM("America/Belem"),
	  BRAZIL_DENORONHA("Brazil/DeNoronha");

	  private String timeZone;

	  TimeZones(String timeZone) {
		 this.timeZone = timeZone;
	  }

	  public String timeZone() {
		 return timeZone;
	  }

   }
}

