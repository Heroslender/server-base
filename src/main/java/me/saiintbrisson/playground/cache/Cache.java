package me.saiintbrisson.playground.cache;

import com.google.common.collect.ImmutableList;
import lombok.Getter;

import java.util.*;
import java.util.function.Predicate;

public abstract class Cache<T> {

	@Getter
	private List<T> elements = new LinkedList();

	public boolean contains(T element) {
		return elements.contains(element);
	}

	public void addElement(T toAdd) {
		elements.add(toAdd);
	}

	public void addElements(List<T> toAdd) {
		elements.addAll(toAdd);
	}

	public void addElements(T... toAdd) {
		addElements(Arrays.asList(toAdd));
	}

	public boolean removeElement(T toRemove) {
		return elements.remove(toRemove);
	}

	public boolean removeElement(Predicate<T> predicate) {
		return getAndRemove(predicate) != null;
	}

	public boolean removeElements(List<T> toRemove) {
		return elements.removeAll(toRemove);
	}

	public boolean removeElements(T... toRemove) {
		return removeElements(Arrays.asList(toRemove));
	}

	public boolean removeElements(Predicate<T> predicate) {
		List<T> toRemove = new ArrayList<>();
		for (T element : elements) {
			if(predicate.test(element)) toRemove.add(element);
		}

		return removeElements(toRemove);
	}

	public T getByIndex(int index) {
		return elements.get(index);
	}

	public T get(Predicate<T> predicate) {
		for (T element : elements)
			if(predicate.test(element)) return element;

		return null;
	}

	public T getAndRemove(Predicate<T> predicate) {
		T element = get(predicate);
		if(element != null) removeElement(element);

		return element;
	}

	public T[] getAll(Predicate<T> predicate) {
		List<T> array = new LinkedList<>();

		for (T element : elements) {
			if(predicate.test(element)) array.add(element);
		}

		return (T[]) array.toArray();
	}

	public Optional<T> find(Predicate<T> predicate) {
		return Optional.ofNullable(get(predicate));
	}

	public Optional<T> findAndRemove(Predicate<T> predicate) {
		Optional<T> optional = find(predicate);
		optional.ifPresent(this::removeElement);

		return optional;
	}

	public List<T> toImmutable() {
		return ImmutableList.copyOf(elements);
	}

	public Iterator<T> iterator() {
		return elements.iterator();
	}

	public int size() {
		return elements.size();
	}

}
