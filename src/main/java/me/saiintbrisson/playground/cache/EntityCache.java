package me.saiintbrisson.playground.cache;

import me.saiintbrisson.playground.holders.EntityHolder;
import org.bukkit.entity.Entity;

import java.util.UUID;

public class EntityCache<T extends EntityHolder> extends Cache<T> {

	public T get(UUID id) {
		return get(it -> it.getId().equals(id));
	}

	public T get(String name) {
		return get(it -> it.getName().equals(name));
	}

	public T get(Entity entity) {
		return get(entity.getUniqueId());
	}

	public boolean removeElement(UUID id) {
		return removeElement(it -> it.getId().equals(id));
	}

	public boolean removeElement(String name) {
		return removeElement(it -> it.getName().equals(name));
	}

	public boolean removeElement(Entity entity) {
		return removeElement(entity.getUniqueId());
	}

}
