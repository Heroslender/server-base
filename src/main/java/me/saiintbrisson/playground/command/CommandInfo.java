package me.saiintbrisson.playground.command;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

@Getter
@RequiredArgsConstructor
public class CommandInfo {

    @NonNull
    private CommandSender sender;
    @NonNull
    private String alias;
    @Setter
    @NonNull
    private String[] args;

    private final Map<CommandParam, String> params = new HashMap<>();

    public void addParam(CommandParam param, String value) {
        params.put(param, value);
    }

    public String getParam(String name) {
        for (Map.Entry<CommandParam, String> entry: params.entrySet()) {
            if(entry.getKey().getName().equals(name)) return entry.getValue();
        }
        return null;
    }

    public String getParam(int index) {
        for (Map.Entry<CommandParam, String> entry: params.entrySet()) {
            if(entry.getKey().getIndex() == index) return entry.getValue();
        }
        return null;
    }

    public Player getPlayer() {
        if(!(sender instanceof Player)) return null;
        return (Player) sender;
    }

    public boolean isPlayer() {
        return sender instanceof Player;
    }

}
