package me.saiintbrisson.playground.command;

import org.apache.commons.lang.StringUtils;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class HelpCommand extends SubCommand {

	public HelpCommand(MainCommand owner) {
		super("?", owner);

		registerAliases("help", "ajuda");
	}

	@Override
	public CommandResponse run(CommandInfo info) {
		CommandSender sender = info.getSender();

		sender.sendMessage(getHelpMessage());
		return returnResponse(ResponseType.SUCCESS);
	}

	@Override
	public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
		if(args.length == 0) return new ArrayList<>();
		String command = args[0].toLowerCase();

		return getSubCommands().stream().filter(it -> {
			if(it.getName().startsWith(command)) return true;
			for (String s : it.getAliases()) {
				if(s.startsWith(command)) return true;
			}
			return false;
		}).map(MainCommand::getName).collect(Collectors.toList());
	}

	public String[] getHelpMessage() {
		List<String> list = new LinkedList<>();

		list.add("");
		list.add("  §6§lAJUDA §7- " + StringUtils.capitalize(getName()));
		list.add("");

		MainCommand owner = getOwnerCommand();
		list.add("  §e/" + getMainCommad().getUsage() + " §f- §7" + owner.getDescription());

		for (SubCommand command : getSubCommands()) {
			String usage = command.getUsage();
			String description = command.getDescription();

			list.add("  §e/" + usage + " §f- §7" + description);
		}

		list.add("");

		return list.toArray(new String[] {});
	}

}
