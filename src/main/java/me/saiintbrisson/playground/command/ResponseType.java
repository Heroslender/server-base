package me.saiintbrisson.playground.command;

public enum ResponseType {

    ERROR,
    SYNTAX_ERROR,
    PERMISSION_ERROR,

    PLAYER_ONLY,
    CONSOLE_ONLY,

    INVALID_PROFILE,
    INVALID_TARGET,

    HELP,

    SUCCESS;

}
