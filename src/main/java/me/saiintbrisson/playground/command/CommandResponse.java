package me.saiintbrisson.playground.command;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CommandResponse {

    private ResponseType type;
    private String message;

}
