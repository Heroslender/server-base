package me.saiintbrisson.playground.command;

import lombok.Getter;
import lombok.NonNull;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public abstract class SubCommand extends MainCommand {

    @Getter
    private MainCommand ownerCommand;

    public SubCommand(String name, @NonNull MainCommand ownerCommand) {
        super(name);
        this.ownerCommand = ownerCommand;
    }

    @Override
    public void setAsync(boolean async) {
        if(isAsync()) return;
        super.setAsync(async);
    }

    @Override
    public boolean isAsync() {
        if(ownerCommand instanceof SubCommand) return ((SubCommand) ownerCommand).isAsync();
        return ownerCommand.isAsync();
    }

    public MainCommand getMainCommad() {
        if(ownerCommand instanceof SubCommand) return ((SubCommand) ownerCommand).getMainCommad();
        return ownerCommand;
    }

    @Override
    public String getUsage() {
        return ownerCommand.getUsage() + " " + super.getUsage() + " ";
    }

    public CommandResponse process(CommandInfo info) {
        CommandSender sender = info.getSender();

        if(getPermission() != null && !testPermission(sender)) {
            return returnResponse(ResponseType.PERMISSION_ERROR);
        }
        if(isPlayerOnly() && !(sender instanceof Player)) {
            return returnResponse(ResponseType.PLAYER_ONLY);
        }

        return run(info);
    }

}
