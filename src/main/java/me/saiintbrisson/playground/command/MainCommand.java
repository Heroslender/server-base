package me.saiintbrisson.playground.command;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Getter
public abstract class MainCommand extends Command {

	private final List<CommandParam> params = new LinkedList<>();
	private final List<SubCommand> subCommands = new ArrayList<>();

	@Setter
	@Getter
	private boolean async, playerOnly;

	public MainCommand(String name) {
		super(name);

		setUsage(name + " ?");
	}

	public void registerAliases(String... aliases) {
		setAliases(Arrays.asList(aliases));
	}

	public void registerParams(String... params) {
		for (String param : params) {
			boolean optional = param.endsWith("?");
			if(optional) param = param.substring(0, param.length() - 1);

			this.params.add(new CommandParam(this.params.size(), param, optional));
		}
	}

	public void registerSubCommands(SubCommand... subCommands) {
		this.subCommands.addAll(Arrays.asList(subCommands));
	}

	public void createHelpCommand() {
		subCommands.add(new HelpCommand(this));
	}

	@Override
	public boolean execute(CommandSender commandSender, String s, String[] strings) {
		CommandInfo info = new CommandInfo(commandSender, s, strings);
		return parseResponse(commandSender, process(info));
	}

	public CommandResponse process(CommandInfo info) {
		CommandSender sender = info.getSender();
		if(getPermission() != null && !testPermission(sender)) {
			sender.sendMessage("§cPermissões insuficientes.");
			return null;
		}
		if(playerOnly && !(sender instanceof Player)) {
			sender.sendMessage("§cApenas jogadores.");
			return null;
		}

		String[] args = info.getArgs();

		if(args.length > 0) {
			SubCommand subCommand = getSubCommand(args[0]);
			if(subCommand != null) {
				info.setArgs(Arrays.copyOfRange(args, 1, args.length));
				return subCommand.process(info);
			}
		}

		for (CommandParam element : params) {
			if(element.isOptional()) continue;
			if(args.length < element.getIndex() + 1) {
				return returnResponse(ResponseType.SYNTAX_ERROR);
			}
		}

		for (int i = 0; i < args.length; i++) {
			String arg = args[i];

			CommandParam param = null;
			for (CommandParam it : params) {
				if(it.getIndex() == i) {
					param = it;
					break;
				}
			}
			if(param == null) continue;

			info.addParam(param, arg);
			if(i + 1 < args.length) {
				SubCommand command = param.getSubCommand(args[i + 1]);
				if(command == null) continue;
				info.setArgs(Arrays.copyOfRange(args, i + 2, args.length));
				return command.process(info);
			}
		}

		if(async) {
			CompletableFuture.runAsync(() -> parseResponse(sender, run(info)));
			return null;
		} else {
			return run(info);
		}
	}

	public abstract CommandResponse run(CommandInfo info);


	public SubCommand getSubCommand(String arg) {
		arg = arg.toLowerCase();
		for (SubCommand subCommand : subCommands) {
			if(subCommand.getName().equals(arg) || subCommand.getAliases().contains(arg)) return subCommand;
		}

		return null;
	}

	public HelpCommand getHelpCommand() {
		for (SubCommand subCommand : subCommands) {
			if(subCommand instanceof HelpCommand) return (HelpCommand) subCommand;
		}

		return null;
	}


	public boolean parseResponse(CommandSender sender, CommandResponse response) {
		if(response == null) return false;

		String message = response.getMessage();

		if(response.getType() == null) {
			if(message != null) sender.sendMessage(message);
			return false;
		}

		switch (response.getType()) {
			case SUCCESS:
				if(message != null) sender.sendMessage("§a" + message + "§a.");
				break;
			case ERROR:
				if(message != null) sender.sendMessage("§c" + message + "§c.");
				else sender.sendMessage("§cErro ao completar pedido.");
				break;
			case PERMISSION_ERROR:
				sender.sendMessage("§cPermissões insuficientes.");
				break;
			case SYNTAX_ERROR:
				sender.sendMessage("§cErro de sintaxe, §e/" + getUsage() + "§c.");
				break;
			case PLAYER_ONLY:
				sender.sendMessage("§cApenas jogadores.");
				break;
			case CONSOLE_ONLY:
				sender.sendMessage("§cApenas console.");
				break;
			case INVALID_PROFILE:
				sender.sendMessage("§cPerfil inválido, tente reconectar.");
				break;
			case INVALID_TARGET:
				sender.sendMessage("§cAlvo inválido, tente outro nome.");
				break;
			case HELP:
				HelpCommand command = getHelpCommand();
				if(command != null) {
					command.run(new CommandInfo(sender, null, null));
				}
				break;
		}

		return response.getType() == ResponseType.SUCCESS;
	}

	public CommandResponse returnResponse(ResponseType type) {
		return returnResponse(type, null);
	}
	public CommandResponse returnResponse(ResponseType type, String message) {
		return new CommandResponse(type, message);
	}
	public CommandResponse returnResponse(String message) {
		return new CommandResponse(null, message);
	}

}
