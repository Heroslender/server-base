package me.saiintbrisson.playground.command;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Getter
@RequiredArgsConstructor
public class CommandParam {

    @NonNull
    private int index;
    @NonNull
    private String name;

    @NonNull
    private boolean optional;

    private final List<SubCommand> subCommands = new ArrayList<>();

    public SubCommand getSubCommand(String arg) {
        arg = arg.toLowerCase();
        for (SubCommand subCommand : subCommands) {
            if(subCommand.getName().equals(arg) || subCommand.getAliases().contains(arg)) return subCommand;
        }

        return null;
    }

}
