package me.saiintbrisson.playground.holders;

import java.util.UUID;

public interface IdHolder {

	UUID getId();

}
