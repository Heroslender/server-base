package me.saiintbrisson.playground.holders;

public interface NameHolder {

	String getName();

}
