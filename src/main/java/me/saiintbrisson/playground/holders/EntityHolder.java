package me.saiintbrisson.playground.holders;

public interface EntityHolder extends IdHolder, NameHolder {
}
