package me.saiintbrisson.playground.builders;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.function.Consumer;

public class ItemBuilder {

	private ItemStack itemStack;
	private ItemMeta itemMeta;

	public ItemBuilder(Material material) {
		itemStack = new ItemStack(material);
		itemMeta = itemStack.getItemMeta();
	}


	public ItemBuilder withItemStack(ItemStack itemStack) {
		this.itemStack = itemStack;
		return this;
	}

	public ItemBuilder withItemMeta(ItemMeta itemMeta) {
		this.itemMeta = itemMeta;
		return this;
	}


	public ItemBuilder acceptItemStack(Consumer<ItemStack> consumer) {
		consumer.accept(itemStack);
		return this;
	}

	public ItemBuilder acceptItemMeta(Consumer<ItemMeta> consumer) {
		consumer.accept(itemMeta);
		return this;
	}


	public ItemBuilder withName(String name) {
		itemMeta.setDisplayName(name);
		return this;
	}

	public ItemBuilder withLore(String... lore) {
		itemMeta.setLore(Arrays.asList(lore));
		return this;
	}

	public ItemBuilder appendLore(String... lore) {
		itemMeta.getLore().addAll(Arrays.asList(lore));
		return this;
	}


	public ItemBuilder withAmount(int amount) {
		itemStack.setAmount(amount);
		return this;
	}

	public ItemBuilder withDurability(short durability) {
		itemStack.setDurability(durability);
		return this;
	}


	public ItemStack build() {
		itemStack.setItemMeta(itemMeta);
		return itemStack;
	}

}
