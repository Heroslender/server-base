package me.saiintbrisson.playground.builders;

import me.saiintbrisson.playground.utils.Functions;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Sasuked
 */
public class EntityBuilder {

   private String customName;
   private EntityType type;
   private ItemStack hand, helmet, chestplate, leggings, boots;
   private List<PotionEffect> potionEffects;

   public EntityBuilder(EntityType type) {
	  this.type = type;
	  this.customName = type.getName();
	  this.potionEffects = new ArrayList<>();
   }

   public EntityBuilder customName(String customName) {
	  this.customName = customName;
	  return this;
   }

   public EntityBuilder hand(ItemStack hand) {
	  this.hand = hand;
	  return this;
   }

   public EntityBuilder helmet(ItemStack helmet) {
	  this.helmet = helmet;
	  return this;
   }

   public EntityBuilder chestplate(ItemStack chestplate) {
	  this.chestplate = chestplate;
	  return this;
   }

   public EntityBuilder boots(ItemStack boots) {
	  this.boots = boots;
	  return this;
   }

   public EntityBuilder withPotionEffect(PotionEffect effect) {
	  this.potionEffects.add(effect);
	  return this;
   }


   public Entity spawn(Location location) {
	  Entity entity = location.getWorld().spawnEntity(location, type);
	  entity.setCustomName(customName);


	  if (entity instanceof LivingEntity) {
		 LivingEntity livingEntity = (LivingEntity) entity;

		 EntityEquipment equipment = livingEntity.getEquipment();
		 Functions.ifNotNull(hand, equipment::setItemInHand);
		 Functions.ifNotNull(helmet, equipment::setHelmet);
		 Functions.ifNotNull(chestplate, equipment::setChestplate);
		 Functions.ifNotNull(leggings, equipment::setLeggings);
		 Functions.ifNotNull(boots, equipment::setBoots);


		 for (PotionEffect potionEffect : potionEffects) {
			livingEntity.addPotionEffect(potionEffect);
		 }

	  }

	  return entity;
   }


}
