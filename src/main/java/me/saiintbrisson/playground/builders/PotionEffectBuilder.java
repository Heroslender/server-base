package me.saiintbrisson.playground.builders;

import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * @author Sasuked
 */
public class PotionEffectBuilder {

   private PotionEffectType type;
   private int seconds;
   private int power;

   public PotionEffectBuilder(PotionEffectType type) {
	  this.type = type;
	  this.seconds = 60;
	  this.power = 1;
   }

   public PotionEffectBuilder seconds(int seconds) {
	  this.seconds = seconds;
	  return this;
   }

   public PotionEffectBuilder power(int power) {
	  this.power = power;
	  return this;
   }

   public PotionEffect build() {
	  return new PotionEffect(type, 20 * seconds, power - 1);
   }
}
