package me.saiintbrisson.playground.storage.database;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import me.saiintbrisson.playground.storage.Credentials;
import me.saiintbrisson.playground.storage.SQLResponse;
import me.saiintbrisson.playground.storage.ServerStatement;
import me.saiintbrisson.playground.utils.ExceptionFunction;

import java.sql.*;

@Getter
@RequiredArgsConstructor
public class MySQLDatabase implements SQLDatabase {

	@NonNull
	private Credentials credentials;

	private Connection connection;
	private boolean active;

	public SQLResponse<Boolean> openConnection() {
		if(active) return new SQLResponse<>(new SQLException("Connection is already opened"));

		try {
			String driver = "com.mysql.jdbc.Driver";
			Class.forName(driver);

			String url = "jdbc:mysql://<ip>:<port>/<database>";

			this.connection = DriverManager.getConnection(
					url.replaceAll("<ip>", credentials.getHost())
							.replaceAll("<port>", String.valueOf(credentials.getPort()))
							.replaceAll("<database>", credentials.getDatabase()),
					credentials.getUsername(), credentials.getPassword()
			);

			DatabaseMetaData metaData = connection.getMetaData();
			System.out.println("Successfully connected to: " + metaData.getDatabaseProductName());
			System.out.println("Driver: " + metaData.getDriverName());

			active = true;
			return new SQLResponse<>(true);
		} catch (Exception e) {
			return new SQLResponse<>(e);
		}
	}

	public void closeConnection() {
		if(!active) return;

		try {
			connection.close();
			active = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public <R> SQLResponse<R> query(String query, ExceptionFunction<ResultSet, R> function, Object... objects) {
		if(!active) return new SQLResponse<>(new SQLException("Connection is closed"));

		try {
			ServerStatement statement = new ServerStatement(connection.prepareStatement(query));
			statement.setObjects(objects);

			ResultSet set = statement.executeQuery();
			if(!set.next())
				return new SQLResponse<>(new SQLException("ResultSet is empty"));

			return new SQLResponse<>(function.apply(set));
		} catch (SQLException e) {
			return new SQLResponse<>(e);
		}
	}

	public SQLResponse<Integer> update(String update, Object... objects) {
		if(!active) return new SQLResponse<>(new SQLException("Connection is closed"));

		try {
			ServerStatement statement = new ServerStatement(connection.prepareStatement(update));
			statement.setObjects(objects);

			return new SQLResponse<>(statement.executeUpdate());
		} catch (SQLException e) {
			return new SQLResponse<>(e);
		}
	}

}
