package me.saiintbrisson.playground.storage.database;

import me.saiintbrisson.playground.storage.Credentials;
import me.saiintbrisson.playground.storage.SQLResponse;
import me.saiintbrisson.playground.utils.ExceptionFunction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface SQLDatabase {
	
	Credentials getCredentials();
	boolean isActive();
	
	Connection getConnection();
	SQLResponse<Boolean> openConnection();
	void closeConnection();
	
	<R> SQLResponse<R> query(String query, ExceptionFunction<ResultSet, R> function, Object... objects);
	SQLResponse<Integer> update(String update, Object... objects);
	
}
