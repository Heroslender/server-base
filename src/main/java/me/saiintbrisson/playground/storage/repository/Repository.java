package me.saiintbrisson.playground.storage.repository;

import me.saiintbrisson.playground.storage.database.SQLDatabase;
import me.saiintbrisson.playground.utils.ExceptionFunction;

import java.sql.ResultSet;

public interface Repository<ID, R> {

	SQLDatabase getDatabase();
	ExceptionFunction<ResultSet, R> getDeserializer();

	R find(ID id);
	int insert(ID id, R r);
	int update(ID id, R r);
	int delete(ID id);

	default AsyncRepository<ID, R> async() {
		return new AsyncRepository<>(this);
	}

}
