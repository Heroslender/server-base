package me.saiintbrisson.playground.storage.repository;

import lombok.AllArgsConstructor;
import lombok.NonNull;

import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

@AllArgsConstructor
public class AsyncRepository<ID, R> {

	private Repository<ID, R> repository;

	public void find(ID id, @NonNull Consumer<R> consumer) {
		runAsync(() -> {
			consumer.accept(repository.find(id));
		});
	}

	public void insert(ID id, R r, Consumer<Integer> consumer) {
		runAsync(() -> {
			Integer result = repository.insert(id, r);
			if(consumer != null) consumer.accept(result);
		});
	}

	public void update(ID id, R r, Consumer<Integer> consumer) {
		runAsync(() -> {
			Integer result = repository.update(id, r);
			if(consumer != null) consumer.accept(result);
		});
	}

	public void delete(ID id, Consumer<Integer> consumer) {
		runAsync(() -> {
			Integer result = repository.delete(id);
			if(consumer != null) consumer.accept(result);
		});
	}

	private void runAsync(Runnable runnable) {
		CompletableFuture.runAsync(runnable);
	}

}
