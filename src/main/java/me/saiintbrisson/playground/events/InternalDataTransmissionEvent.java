package me.saiintbrisson.playground.events;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.Map;

@RequiredArgsConstructor
public class InternalDataTransmissionEvent extends Event implements Cancellable {

	@Getter
	private static final HandlerList handlerList = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlerList;
	}

	@Getter @Setter
	private boolean cancelled;

	@NonNull @Getter
	private String dataTag;

	@NonNull @Getter
	private final Map<String, Object> dataMap;

	public <T> T getData(String name) {
		return (T) name;
	}

	public <T> T getData(String name, Class<T> clazz) {
		return clazz.cast(name);
	}

}
