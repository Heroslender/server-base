package me.saiintbrisson.playground.server;

import me.saiintbrisson.playground.utils.Reflections;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class ServerPlugin extends JavaPlugin {

	public void registerCommands(Command... commands) {
		CommandMap map = Reflections.getCommandMap();
		for (Command command : commands) {
			map.register(getName(), command);
		}
	}

	public void registerListeners(Listener... listeners) {
		for (Listener listener : listeners) {
			Bukkit.getPluginManager().registerEvents(listener, this);
		}
	}

}
