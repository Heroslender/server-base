package me.saiintbrisson.playground.server;

import java.util.ArrayList;
import java.util.List;

public final class Services {

	private static final List<Object> SERVICES = new ArrayList<>();

	/**
	 * Registers and saves a new Service instance to {@link Services#SERVICES}
	 *
	 * @param instance the Service instance
	 * @param serviceClass the Service class
	 * @param <T> the Service type
	 */
	public static <T> void provide(T instance, Class<T> serviceClass) {
		if(isProvided(serviceClass)) {
			throw new IllegalAccessError("Service already provided.");
		}

		SERVICES.add(instance);
	}

	/**
	 * Returns a Service matching the given Class
	 *
	 * @param serviceClass the Service class
	 * @param <T> the Service type
	 * @return the service matching the given Class
	 */
	public static <T> T get(Class<T> serviceClass) {
		for (Object service : SERVICES) {
			if(serviceClass.isInstance(service)) return serviceClass.cast(service);
		}

		return null;
	}

	/**
	 * Verifies if Service is already provided
	 *
	 * @param serviceClass the Service class
	 * @param <T> the Service type
	 * @return if the Service is already provided
	 */
	public static <T> boolean isProvided(Class<T> serviceClass){
		return SERVICES.stream().anyMatch(serviceClass::isInstance);
	}

	public static <T> void unregisterService(Class<T> serviceClass) {
		T service = get(serviceClass);
		if(service == null) return;

		SERVICES.remove(service);
	}

	public static void unregisterAll() {
		SERVICES.clear();
	}

}
